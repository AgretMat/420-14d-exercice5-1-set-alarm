package com.dinfogarneau.tp.alarm_clock;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private Button btSetAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btSetAlarm = findViewById(R.id.bt_set_alarm);

        btSetAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Integer> week = new ArrayList<>();
                week.add(Calendar.MONDAY);
                week.add(Calendar.TUESDAY);
                week.add(Calendar.WEDNESDAY);

                // https://developer.android.com/guide/components/intents-common#Clock
                Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                        .putExtra(AlarmClock.EXTRA_MESSAGE, "Go go go !")
                        .putExtra(AlarmClock.EXTRA_HOUR, 17)
                        .putExtra(AlarmClock.EXTRA_MINUTES, 0)
                        .putExtra(AlarmClock.EXTRA_DAYS, week);
                startActivity(Intent.createChooser(intent, "Choose Application"));
            }
        });

    }
}